path = 'X';   %INSERT JSONLAB LIBRARY LOCATION (for example C:\Users\Student\Desktop\jsonlab-1.9)
addpath(path);
clc  %%Clear Command Window
clear %%Clear Workspace

IntervalTime = 3600   %Loop interval used to specify time between the data update. 3600 is equal to 3600 seconds that is equal of 1 hour.
while true %While loop with true argument
   tic %tic starts a stopwatch timer to measure performance. The function records the internal time at execution of the tic command

katowice=loadjson(urlread ('https://danepubliczne.imgw.pl/api/data/synop/station/katowice/format/json')); %Read url via json library and put it into katowice variable 
czestochowa=loadjson(urlread ('https://danepubliczne.imgw.pl/api/data/synop/station/czestochowa/format/json'));
krakow=loadjson(urlread ('https://danepubliczne.imgw.pl/api/data/synop/station/krakow/format/json'));

file = 'weatherData.csv';  %Create csv file
header = {'Stacja', 'Data (RR-MM-DD)', 'Godzina (H)', 'Temperatura (C)', 'Cisnienie (hPa)', 'Predkosc wiatru (km/h)', 'Wilgotnosc (%)', 'Temperatura odczuwalna (C)'}; %Set the header of the CSV file
header_station = {'IMGW ', ' .', '. ', ' .', ' .', ' .', '. ', '. '};
header_station_1 = {'WTTR ', ' .', '. ', ' .', ' .', ' .', '. ', '. '};
header_space = {' ', '  ', '  ', '  ', '  ', '  ', '  ', '  '};
data = fopen(file, 'w+');
fprintf(data, '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n','', header{1}, ';', header{2}, ';', header{3}, ';', header{4}, ';', header{5}, ';', header{6}, ';', header{7},';', header{8}, '');  %Writing data into csv file
fprintf(data, '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '', header_station{1}, ';', header_station{2}, ';', header_station{3}, ';', header_station{4}, ';', header_station{5}, ';', header_station{6}, ';', header_station{7}, ';', header_station{8}, '');
fclose(data)  %Closing the file

Katowice=cell(7,2)  %Creating a matrix size of 7x2 to store data
Katowice{1,2} = katowice.data_pomiaru; %Read date when the reading's were uploaded and put it into cell number (1,2)
Katowice{2,2} = str2num(katowice.godzina_pomiaru); %Read time when the reading's were uploaded and put it into cell number (2,2)
Katowice{3,2} = str2num(katowice.temperatura); %Read temperature and put it into cell number (3,2)
Katowice{4,2} = str2num(katowice.cisnienie);
Katowice{5,2} = str2num(katowice.predkosc_wiatru);
Katowice{6,2} = str2num(katowice.wilgotnosc_wzgledna);
Katowice{7,2} = (13.12+((0.6215*Katowice{3,2})-(11.37*(Katowice{5,2}^0.16))+(0.3965*Katowice{3,2}*(Katowice{5,2}^0.16)))); %Calculate the windchill using the North American and United Kingdom wind chill index

Krakow=cell(7,2)
Krakow{1,2} = krakow.data_pomiaru;
Krakow{2,2} = str2num(krakow.godzina_pomiaru);
Krakow{3,2} = str2num(krakow.temperatura);
Krakow{4,2} = str2num(krakow.cisnienie);
Krakow{5,2} = str2num(krakow.predkosc_wiatru);
Krakow{6,2} = str2num(krakow.wilgotnosc_wzgledna);
Krakow{7,2} = (13.12+((0.6215*Krakow{3,2})-(11.37*(Krakow{5,2}^0.16))+(0.3965*Krakow{3,2}*(Krakow{5,2}^0.16))));


Czestochowa=cell(7,2)
Czestochowa{1,2} = czestochowa.data_pomiaru;
Czestochowa{2,2} = str2num(czestochowa.godzina_pomiaru);
Czestochowa{3,2} = str2num(czestochowa.temperatura);
Czestochowa{4,2} = str2num(czestochowa.cisnienie);
Czestochowa{5,2} = str2num(czestochowa.predkosc_wiatru);
Czestochowa{6,2} = str2num(czestochowa.wilgotnosc_wzgledna);
Czestochowa{7,2} = (13.12+((0.6215*Czestochowa{3,2})-(11.37*(Czestochowa{5,2}^0.16))+(0.3965*Czestochowa{3,2}*(Czestochowa{5,2}^0.16))));


data = fopen(file, 'a');
fprintf(data,'%s%s%s%s%s%.0f%.1s%.1f%.1s%.1f%.1s%.1f%.1s%.1f%.1s%.1f\n','','Katowice',';',Katowice{1,2},';',Katowice{2,2},';',Katowice{3,2},';',Katowice{4,2},';',Katowice{5,2},';',Katowice{6,2},';',Katowice{7,2},'')
fprintf(data, '%s%s%s%s%s%.0f%.1s%.1f%.1s%.1f%.1s%.1f%.1s%.1f%.1s%.1f\n','','Krakow',';',Krakow{1,2},';',Krakow{2,2},';',Krakow{3,2},';',Krakow{4,2},';',Krakow{5,2},';',Krakow{6,2},';',Krakow{7,2},'');
fprintf(data, '%s%s%s%s%s%.0f%.1s%.1f%.1s%.1f%.1s%.1f%.1s%.1f%.1s%.1f\n','','Czestochowa',';',Czestochowa{1,2},';',Czestochowa{2,2},';',Czestochowa{3,2},';',Czestochowa{4,2},';',Czestochowa{5,2},';',Czestochowa{6,2},';',Czestochowa{7,2},'');
fprintf(data, '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '', header_space{1}, ';', header_space{2}, ';', header_space{3}, ';', header_space{4}, ';', header_space{5}, ';', header_space{6}, ';', header_space{7}, ';', header_space{8}, '');
fprintf(data, '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n','', header{1}, ';', header{2}, ';', header{3}, ';', header{4}, ';', header{5}, ';', header{6}, ';', header{7},';', header{8}, '');
fprintf(data, '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n', '', header_station_1{1}, ';', header_station_1{2}, ';', header_station_1{3}, ';', header_station_1{4}, ';', header_station_1{5}, ';', header_station_1{6}, ';', header_station_1{7}, ';', header_station_1{8}, '');
fclose(data)

dane_krakow_wttr = loadjson(urlread('http://wttr.in/krakow?format=j1')); %Read the url from wttr site
dane_katowice_wttr = loadjson(urlread('http://wttr.in/katowice?format=j1'));
dane_czestochowa_wttr = loadjson(urlread('http://wttr.in/czestochowa?format=j1'));

Krakow_wttr = cell(7,2);

Krakow_wttr{1,2} = dane_krakow_wttr.weather{1,1}.date;
Krakow_wttr{2,2} = dane_krakow_wttr.current_condition{1,1}.observation_time;
Krakow_wttr{3,2} = str2num(dane_krakow_wttr.current_condition{1,1}.temp_C);
Krakow_wttr{4,2} = str2num(dane_krakow_wttr.current_condition{1,1}.pressure);
Krakow_wttr{5,2} = str2num(dane_krakow_wttr.current_condition{1,1}.windspeedKmph);
Krakow_wttr{6,2} = str2num(dane_krakow_wttr.current_condition{1,1}.humidity);
Krakow_wttr{7,2} = str2num(dane_krakow_wttr.current_condition{1,1}.FeelsLikeC);

Krakow_wttr = cell(7,2);

Krakow_wttr{1,2} = dane_krakow_wttr.weather{1,1}.date;
Krakow_wttr{2,2} = dane_krakow_wttr.current_condition{1,1}.observation_time;
Krakow_wttr{3,2} = str2num(dane_krakow_wttr.current_condition{1,1}.temp_C);
Krakow_wttr{4,2} = str2num(dane_krakow_wttr.current_condition{1,1}.pressure);
Krakow_wttr{5,2} = str2num(dane_krakow_wttr.current_condition{1,1}.windspeedKmph);
Krakow_wttr{6,2} = str2num(dane_krakow_wttr.current_condition{1,1}.humidity);
Krakow_wttr{7,2} = str2num(dane_krakow_wttr.current_condition{1,1}.FeelsLikeC);

Katowice_wttr = cell(7,2);

Katowice_wttr{1,2} = dane_katowice_wttr.weather{1,1}.date;
Katowice_wttr{2,2} = dane_katowice_wttr.current_condition{1,1}.observation_time;
Katowice_wttr{3,2} = str2num(dane_katowice_wttr.current_condition{1,1}.temp_C);
Katowice_wttr{4,2} = str2num(dane_katowice_wttr.current_condition{1,1}.pressure);
Katowice_wttr{5,2} = str2num(dane_katowice_wttr.current_condition{1,1}.windspeedKmph);
Katowice_wttr{6,2} = str2num(dane_katowice_wttr.current_condition{1,1}.humidity);
Katowice_wttr{7,2} = str2num(dane_katowice_wttr.current_condition{1,1}.FeelsLikeC);

Czestochowa_wttr = cell(7,2);

Czestochowa_wttr{1,2} = dane_czestochowa_wttr.weather{1,1}.date;
Czestochowa_wttr{2,2} = dane_czestochowa_wttr.current_condition{1,1}.observation_time;
Czestochowa_wttr{3,2} = str2num(dane_czestochowa_wttr.current_condition{1,1}.temp_C);
Czestochowa_wttr{4,2} = str2num(dane_czestochowa_wttr.current_condition{1,1}.pressure);
Czestochowa_wttr{5,2} = str2num(dane_czestochowa_wttr.current_condition{1,1}.windspeedKmph);
Czestochowa_wttr{6,2} = str2num(dane_czestochowa_wttr.current_condition{1,1}.humidity);
Czestochowa_wttr{7,2} = str2num(dane_czestochowa_wttr.current_condition{1,1}.FeelsLikeC);



data = fopen(file, 'a');
fprintf(data, '%s%s%s%s%s%s%s%.1f%s%.1f%s%.1f%s%.1f%s%.1f%s\n','"','Krakow','";"',Krakow_wttr{1,2},'";"',Krakow_wttr{2,2},'";"',Krakow_wttr{3,2},'";"',Krakow_wttr{4,2},'";"',Krakow_wttr{5,2},'";"',Krakow_wttr{6,2},'";"',Krakow_wttr{7,2},'"');
fprintf(data, '%s%s%s%s%s%s%s%.1f%s%.1f%s%.1f%s%.1f%s%.1f%s\n','"','Czestochowa','";"',Czestochowa_wttr{1,2},'";"',Czestochowa_wttr{2,2},'";"',Czestochowa_wttr{3,2},'";"',Czestochowa_wttr{4,2},'";"',Czestochowa_wttr{5,2},'";"',Czestochowa_wttr{6,2},'";"',Czestochowa_wttr{7,2},'"');
fprintf(data, '%s%s%s%s%s%s%s%.1f%s%.1f%s%.1f%s%.1f%s%.1f%s\n','"','Katowice','";"',Katowice_wttr{1,2},'";"',Katowice_wttr{2,2},'";"',Katowice_wttr{3,2},'";"',Katowice_wttr{4,2},'";"',Katowice_wttr{5,2},'";"',Katowice_wttr{6,2},'";"',Katowice_wttr{7,2},'"');
fclose(data)


CombineMatrixOfTempMax=[Czestochowa_wttr{3,2},Katowice_wttr{3,2},Krakow_wttr{3,2},Czestochowa{3,2},Katowice{3,2},Krakow{3,2}];  %Combine all temperature cells into one matrix
MaxTemp = max(CombineMatrixOfTempMax(:)); %Find maximum temperature 
disp('Max Temp') %Display value in console
disp(MaxTemp)   %Display value in console

CombineMatrixOfTempMin=[Czestochowa_wttr{3,2},Katowice_wttr{3,2},Krakow_wttr{3,2},Czestochowa{3,2},Katowice{3,2},Krakow{3,2}];  %Combine all temperature cells into one matrix
MinTemp = min(CombineMatrixOfTempMin(:)); %Find mninimum temperature 
disp('Min Temp')  %Display value in console
disp(MinTemp) %Display value in console



CombineMatrixOfWindChillMax=[Czestochowa_wttr{7,2},Katowice_wttr{7,2},Krakow_wttr{7,2},Czestochowa{7,2},Katowice{7,2},Krakow{7,2}];
MaxWindChill = max(CombineMatrixOfWindChillMax(:));
disp('Max WindChill')
disp(MaxWindChill)

CombineMatrixOfWindChillMin=[Czestochowa_wttr{7,2},Katowice_wttr{7,2},Krakow_wttr{7,2},Czestochowa{7,2},Katowice{7,2},Krakow{7,2}];
MinWindChill = min(CombineMatrixOfWindChillMin(:));
disp('Min WindChill')
disp(MinWindChill)



CombineMatrixOfPressureMax=[Czestochowa_wttr{4,2},Katowice_wttr{4,2},Krakow_wttr{4,2},Czestochowa{4,2},Katowice{4,2},Krakow{4,2}];
MaxPressure = max(CombineMatrixOfPressureMax(:));
disp('Max Pressure')
disp(MaxPressure)

CombineMatrixOfPressureMin=[Czestochowa_wttr{4,2},Katowice_wttr{4,2},Krakow_wttr{4,2},Czestochowa{4,2},Katowice{4,2},Krakow{4,2}];
MinPressure = min(CombineMatrixOfPressureMin(:));
disp('Min Pressure')
disp(MinPressure)



CombineMatrixOfhumidityMax=[Czestochowa_wttr{6,2},Katowice_wttr{6,2},Krakow_wttr{6,2},Czestochowa{6,2},Katowice{6,2},Krakow{6,2}];
Maxhumidity = max(CombineMatrixOfhumidityMax(:));
disp('Max humidity')
disp(Maxhumidity)

CombineMatrixOfhumidityMin=[Czestochowa_wttr{6,2},Katowice_wttr{6,2},Krakow_wttr{6,2},Czestochowa{6,2},Katowice{6,2},Krakow{6,2}];
Minhumidity = min(CombineMatrixOfhumidityMin(:));
disp('Min humidity')
disp(Minhumidity)



elapsedTime=toc  %print elapsed time
   pause(IntervalTime+elapsedTime); %Pause the loop for the specified amount of time
   clearvars elapsedTime; %clear time form memory
end  %end of the loop
